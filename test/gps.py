import serial
import pynmea2


def parse_gps(data):
    if data.find('GGA') > 0:
        msg = pynmea2.parse(data)
        if msg.lat != '':
            print "Timestamp: %s -- Lat: %s %s -- Lon: %s %s -- Altitude: %s %s" % (
                msg.timestamp, float(msg.lat) / 100, msg.lat_dir, float(msg.lon) / 100, msg.lon_dir,
                msg.altitude, msg.altitude_units)


serialPort = serial.Serial("/dev/ttyS0", 9600, timeout=0.5)

while True:
    line_data = serialPort.readline()
    parse_gps(line_data)
