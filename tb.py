import time
import threading
from Queue import Queue

import shutil

import os

from settings import *
from src.utils.camera import CameraService
from src.utils.epaper import EPD2Service, EPD4Service


def _epd4_task():
    epd4 = EPD4Service()
    # clear_epd4()
    # display_clerk_logo()
    # print 'EPD2: Displayed Lazlo logo.'
    # time.sleep(2)
    # display_qr_code('This is the example QR Code to be scanned')
    pass


def _epd2_task():
    epd2 = EPD2Service()
    # epd2.clear()
    # epd2.display_text([
    #     {'text': 'Oops! No Wifi signal!', 'font_size': 20, 'left': 25, 'top': 30},
    #     {'text': 'Please reboot and scan', 'font_size': 17, 'left': 40, 'top': 70},
    #     {'text': 'your Wifi QrCode.', 'font_size': 17, 'left': 70, 'top': 90}
    # ])
    # epd2.display_text([
    #     {'text': "Oops! You're not in", 'font_size': 23, 'left': 20, 'top': 30},
    #     {'text': "a valid location!", 'font_size': 23, 'left': 20, 'top': 70},
    # ])
    # epd2.display_lazlo_logo()
    # print 'EPD2: Displayed Lazlo logo.'
    # time.sleep(2)
    # print 'EPD2: Now, displaying barcode.'
    epd2.display_upc12_code('012000018800', amount_due=5.04, wifi_icon=True)


# threading.Thread(target=_epd4_task).start()
# threading.Thread(target=_epd2_task).start()

print 'Upload face: ', UPLOAD_FACE

queue = Queue()
inst = CameraService(queue=queue)
inst.start()
while True:
    if queue.qsize() > 0:
        img_file = queue.get()
        shutil.copy(img_file, os.path.expanduser('~/Pictures'))


# import cv2
# frame = cv2.imread(os.path.expanduser('~/Pictures/1.jpg'))
# cv2.imshow('Found Face', frame)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
