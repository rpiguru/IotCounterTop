import datetime
import json
import ntpath
import sys
import threading
import time
from Queue import Queue
from iothub_client import IoTHubClient
from iothub_client import IoTHubMessageDispositionResult, DeviceMethodReturnValue, IoTHubTransportProvider

from settings import *
from src.utils.azure import upload_image_to_azure_iot_hub
from src.utils.beacon import start_beacon
from src.utils.common import is_rpi, connect_to_ap, check_internet_connection, get_mac_address
from src.utils.epaper import EPD2Service, EPD4Service
from src.utils.gps import GPSService
from src.utils.service import get_config, get_license_code, checkout

if is_rpi():
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)


MESSAGE_SWITCH = True


def on_device_twin_from_azure_iot(update_state, payload, user_context):
    print '+++++ Twin callback is called +++++'
    print " updateStatus = %s\n payload = %s\n context = %s\n" % (update_state, payload, user_context)


def on_device_method_from_azure_iot(method_name, payload, user_context):
    global MESSAGE_SWITCH
    print "<<<<< Method callback called <<<<<"
    print " methodName = %s\n payload = %s\n context = %s\n" % (method_name, payload, user_context)
    device_method_return_value = DeviceMethodReturnValue()
    device_method_return_value.response = "{ \"Response\": \"This is the response from the device\" }"
    device_method_return_value.status = 200
    if method_name == "start":
        MESSAGE_SWITCH = True
        print ("Start sending message\n")
        device_method_return_value.response = "{ \"Response\": \"Successfully started\" }"
        return device_method_return_value
    if method_name == "stop":
        MESSAGE_SWITCH = False
        print ("Stop sending message\n")
        device_method_return_value.response = "{ \"Response\": \"Successfully stopped\" }"
        return device_method_return_value
    return device_method_return_value


class IoTCounterTop(threading.Thread):

    conn_string = None              # Connection String for Azure IoT Hub
    lic_code = None                 # licenseCode
    cor_id = None                   # correlationRefId
    beacon_config = {}              # Beacon data(major, minor, name, uuid)
    polygon = []                    # geo-fence polygon

    lock = threading.RLock()
    epd2_lock = threading.RLock()
    epd4_lock = threading.RLock()
    epd2_queue = Queue()
    epd4_queue = Queue()
    face_queue = Queue()

    pause = threading.Event()
    client = None
    wifi_status = False

    def __init__(self):
        super(IoTCounterTop, self).__init__()
        self.epd2 = EPD2Service()
        self.epd4 = EPD4Service()
        self.gps = GPSService()
        self.camera = None
        threading.Thread(target=self.epd2_worker).start()
        threading.Thread(target=self.epd4_worker).start()

    def initialize(self):
        # 1. Display splash images
        self.epd2_queue.put({'type': 'clear'})
        self.epd2_queue.put({'type': 'logo'})
        self.epd4_queue.put({'type': 'clear'})
        self.epd4_queue.put({'type': 'logo'})

        time.sleep(2)
        while self.epd4.is_busy():
            time.sleep(.5)

        # 2. Scan WiFi QR code for 5 sec
        from src.utils.camera import CameraService
        self.camera = CameraService(queue=self.face_queue, path='faces')

        wifi_info = self.scan_wifi_qr_code()
        if wifi_info:
            new_ip = connect_to_ap(ssid=wifi_info['ssid'], pwd=wifi_info['password'])
            if new_ip:
                logger.info('Successfully connected to {}, new IP: {}'.format(wifi_info['ssid'], new_ip))

        # 3. Check internet connectivity
        self.wifi_status = check_internet_connection()
        if not self.wifi_status:
            self.epd2_queue.put({
                'type': 'text',
                'data': [
                    {'text': 'Oops! No Wifi signal!', 'font_size': 20, 'left': 25, 'top': 30},
                    {'text': 'Please reboot and scan', 'font_size': 17, 'left': 40, 'top': 70},
                    {'text': 'your Wifi QrCode.', 'font_size': 17, 'left': 70, 'top': 90}
                ]
            })
            if not DEBUG:
                self.camera.stop()
                return False

        config = get_config()
        if config is None:
            logger.error('!!! Invalid config - {}'.format(config))
            sys.exit(1)
        self.beacon_config = {
            'major': config['data']['beaconMajor'],
            'minor': config['data']['beaconMinor'],
            'name': str(config['data']['beaconName']),
            'uuid': str(config['data']['beaconUuid'])
        }
        threading.Thread(target=self.start_beacon).start()

        self.polygon = [int(float(p)) for p in str(config['data']['geoFencePolygon']).split(',')]
        logger.info('Fence Polygon: {}'.format(self.polygon))
        self.conn_string = str(config.get('data', {}).get('iotConnectionString'))
        self.client = IoTHubClient(self.conn_string, IoTHubTransportProvider.MQTT)
        self.client.set_option("product_info", "IoT Counter Top")
        # Set the time until a message times out
        self.client.set_option("messageTimeout", AZURE_IOT_MESSAGE_TIMEOUT)
        # Enable MQTT logging
        self.client.set_option("logtrace", 0)
        self.client.set_message_callback(self.on_message_received_from_azure_iot, AZURE_IOT_RECEIVE_CONTEXT)
        # client.set_device_twin_callback(on_device_twin_from_azure_iot, AZURE_IOT_TWIN_CONTEXT)
        # client.set_device_method_callback(on_device_method_from_azure_iot, AZURE_IOT_METHOD_CONTEXT)

        lic_data = get_license_code()
        if lic_data:
            self.lic_code = str(lic_data['licenseCode'])
            self.cor_id = str(lic_data['correlationRefId'])
        else:
            logger.critical('Failed to received license code from the server! Exiting...')
            sys.exit(1)
        self.epd4_queue.put({'type': 'qrcode', 'data': self.lic_code})

        self.gps.start()

        if is_rpi():
            GPIO.add_event_detect(BUTTON_PIN, GPIO.FALLING, callback=self.on_button_pressed, bouncetime=200)

        return True

    def scan_wifi_qr_code(self):
        s_time = time.time()
        while True:
            wifi_info = self.camera.scan_wifi_qr_code()
            if wifi_info:
                logger.info('Successfully scanned WiFi QR Code: {}'.format(wifi_info))
                return wifi_info
            if time.time() - s_time > WIFI_QR_SCAN_TIME:
                logger.warning('Failed to scan WiFi QR code in {} sec.'.format(WIFI_QR_SCAN_TIME))
                return
            time.sleep(.1)

    def run(self):
        self.camera.start()
        threading.Thread(target=self.update_license_code).start()
        threading.Thread(target=self.check_detected_faces).start()
        while True:
            if not self.gps.validate(self.polygon):
                logger.error('Oops! You\'re not in a valid location!')
                with self.epd2_lock:
                    self.epd2_queue.put({
                        'type': 'text',
                        'data': [
                            {'text': "Oops! You're not in", 'font_size': 23, 'left': 20, 'top': 30},
                            {'text': "a valid location!", 'font_size': 23, 'left': 20, 'top': 70},
                        ]
                    })
            time.sleep(GPS_CHECK_INTERVAL * 60)

    def on_message_received_from_azure_iot(self, message, counter):
        message_buffer = message.get_bytearray()
        size = len(message_buffer)
        logger.info('=== Received a message({}) from Azure IoT Hub'.format(counter))
        data = message_buffer[:size].decode("utf-8")
        logger.info('  Data: `{}`, size: `{}`'.format(data, size))
        try:
            json_data = json.loads(data)
            new_code = json_data.get('gtin12')
            amount_due = json_data.get('amount')
            logger.info("  New Barcode: %s, Amount Due: %s" % (new_code, amount_due))
            if new_code:
                with self.epd2_lock:
                    self.epd2_queue.put({
                        'type': 'gtin-12',
                        'code': str(new_code),
                        'amount_due': amount_due
                    })
        except ValueError:
            logger.error('Invalid message - ```{}```'.format(data))
        return IoTHubMessageDispositionResult.ACCEPTED

    def check_detected_faces(self):
        while True:
            if not self.pause.isSet():
                if self.face_queue.qsize() > 0:
                    img_file = self.face_queue.get()
                    if UPLOAD_FACE:
                        self.upload_file_to_iot_hub(img_file)
            time.sleep(.1)

    def upload_file_to_iot_hub(self, file_path):
        file_name = ntpath.basename(file_path)
        upload_image_to_azure_iot_hub(img_path=file_path, file_name=file_name, connection_string=self.conn_string)

    def on_button_pressed(self, *args):
        logger.info('Button is pressed, checking out...')
        with self.lock:
            lat, lon = self.gps.get_position()
            data = {
                "correlationRefId": self.cor_id,
                "uuid": get_mac_address(),
                "latitude": lat,
                "longitude": lon,
                "createdOn": datetime.datetime.now().isoformat(),
                "data": {"licenseCode": self.lic_code}
            }
        checkout(data)

    def update_license_code(self):
        while True:
            if not self.pause.isSet():
                lic_data = get_license_code()
                if lic_data:
                    with self.lock:
                        lic_code = str(lic_data['licenseCode'])
                        cor_id = str(lic_data['correlationRefId'])
                        if lic_code != self.lic_code or self.cor_id != cor_id:
                            logger.info('New License Code & CorrelationID received - {}/{}'.format(lic_code, cor_id))
                            self.lic_code = lic_code
                            self.cor_id = cor_id
                            with self.epd4_lock:
                                self.epd4_queue.put({'type': 'qrcode', 'data': self.lic_code})
            time.sleep(1)

    def epd2_worker(self):
        while True:
            if not self.pause.isSet():
                if self.epd2_queue.qsize() > 0:
                    data = self.epd2_queue.get()
                    if data['type'] == 'logo':
                        self.epd2.display_lazlo_logo()
                    elif data['type'] == 'clear':
                        self.epd2.clear()
                    elif data['type'] == 'text':
                        self.epd2.display_text(data['data'], wifi_icon=self.wifi_status)
                    elif data['type'] == 'gtin-12':
                        self.epd2.display_upc12_code(code=data['code'], amount_due=data['amount_due'],
                                                     wifi_icon=self.wifi_status)
            time.sleep(.1)

    def epd4_worker(self):
        while True:
            if not self.pause.isSet():
                if self.epd4_queue.qsize() > 0:
                    data = self.epd4_queue.get()
                    if data['type'] == 'logo':
                        self.epd4.display_clerk_logo()
                    elif data['type'] == 'clear':
                        self.epd4.clear()
                    elif data['type'] == 'text':
                        # self.epd4.display_text(data['data'])
                        pass
                    elif data['type'] == 'qrcode':
                        self.epd4.display_qr_code(data['data'])
            time.sleep(.1)

    def start_beacon(self):
        start_beacon(uuid=self.beacon_config['uuid'],
                     major=self.beacon_config['major'], minor=self.beacon_config['minor'])


if __name__ == '__main__':

    logger.info('=============== Starting ===============')

    app = IoTCounterTop()

    init = app.initialize()

    if init:
        app.start()
    else:
        while True:
            time.sleep(1)
