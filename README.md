# IoT Counter Top

## Components

- https://www.adafruit.com/product/3400

- https://www.adafruit.com/product/746

- https://www.adafruit.com/product/1333

- https://www.adafruit.com/product/328

- https://www.waveshare.com/wiki/4.2inch_e-Paper_Module_(B)

- https://www.waveshare.com/product/2.9inch-e-paper-module-b.htm


## Installation


### Connection

- RPi and GPS module

| **GPS**  | **RPi**   |
| ----     |:-------:  |
| GND      |    GND    |
| VIN      |    5V     |
| TxD      |  GPIO 15  |
| RxD      |  GPIO 14  |

- RPi and 4.2inch e-Paper

| **e-Paper** |     **RPi**   |
|    ----     |    :-------:  |
|    GND      |    GND        |
|    VCC      |    3.3V       |
|    DIN      | GPIO 10(MOSI) |
|    CLK      | GPIO 11(SCLK) |
|    CS       | GPIO 8 (CS0)  |
|    RST      |    GPIO 17    |
|    DC       |    GPIO 27    |
|    BUSY     |    GPIO 22    |

- RPi and 2.9inch e-Paper

| **e-Paper** |     **RPi**   |
|    ----     |    :-------:  |
|    GND      |    GND        |
|    VCC      |    3.3V       |
|    DIN      | GPIO 20(MOSI) |
|    CLK      | GPIO 21(SCLK) |
|    CS       | GPIO 18 (CS1) |
|    RST      |    GPIO 5     |
|    DC       |    GPIO 6     |
|    BUSY     |    GPIO 13    |

- RPi and Push Button

| **Button** |     **RPi**   |
|    ----    |    :-------:  |
|    BTN     |    GPIO 25    |
|    GND     |    GND        |
|    VCC     |    3.3V       |

Use a resistor to make this button pull up.

https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/robot/buttons_and_switches/


### Now, install everything on RPi:

  **NOTE** This will take about 10 hours, so please be patient!

    cd scripts
    chmod +x install.sh
    ./install.sh

## Scenario

1. On boot display splash image(s)

2. On splash complete, camera scans for Wifi qrcode for 5 seconds, if not scanned, configures default static ssid, if scanned and not present or no internet, display "Oops! No Wifi signal. Please reboot and scan your Wifi QrCode." on smaller epaper display.

3. On Wifi internet connectivity, display Wifi icon to footer of smaller display.

Async after Wifi internet connectivity, scan faces on camera. save face rectangles to local storage, upload to storage (tbd next phase)

4. call GET https://dev.services.32point6.com/api/v1/admin/iot/config/00-14-22-01-23-45

    http header:

        Lazlo-AuthorityLicenseCode: n1oJ6u4cKeQwCvGg5ZrxHJaMOoQ=:VEMZGC9M7HL72RQYAAY94MJRPNGMLLN9R34ZM8W3QEJ935BH3GFQ1EQH4N7VPZH56F1AA4VV2UYRGGJR6NRRYDFV6NTXS8UZNNQ324RC6NRRGGUZXUURA4RZX6AXHA

5. Then call GET https://dev.services.32point6.com/api/v1/admin/iot/license/00-14-22-01-23-45

    and response is:

        {
            "correlationRefId": "00000000-0000-0000-0000-000000000000",
            "createdOn": "2017-11-14T20:32:05.7691317+00:00",
            "error": null,
            "data": {
                "licenseCode": "D4fiYCYj6mmmSpgclwiqtRmU3ns=:7DQSRWNDKHSGQYSD6GWJTHT5N76HFZ84AKTN74EXHP7VDCAMYC5Q1EQHGKT67VYHVX1AZ6UT28TXZ6FWNNTXBGRX6NTXUGUCNNW3X6JT6NYRUJFR6T9XS4RWXKWXNA"
            }
        }

    display `licenseCode` as qrcode to epaper(Large one with 400x300).

6. Save `licenseCode` temp and then listen on iot hub for message...

7. on receive: display `gtin` as barcode to epaper display

8. button press sends this PUT https://dev.services.32point6.comapi/v2/shopping/iot/checkout/complete

    http header:

        Lazlo-AuthorityLicenseCode: n1oJ6u4cKeQwCvGg5ZrxHJaMOoQ=:VEMZGC9M7HL72RQYAAY94MJRPNGMLLN9R34ZM8W3QEJ935BH3GFQ1EQH4N7VPZH56F1AA4VV2UYRGGJR6NRRYDFV6NTXS8UZNNQ324RC6NRRGGUZXUURA4RZX6AXHA

    PUT body:

        {
            "correlationRefId": "{from https://dev.services.32point6.com/api/v1/admin/iot/license/00-14-22-01-23-45 response}",
            "uuid": "{mac address} test: 00-14-22-01-23-45",
            "latitude": {current} test: 42.136268881127663,
            "longitude": {current} test: -80.094999131507038,
            "createdOn": "{current}",
            "data": {
                "licenseCode": {from https://dev.services.32point6.com/api/v1/admin/iot/license/00-14-22-01-23-45 response} test:  "D4fiYCYj6mmmSpgclwiqtRmU3ns=:7DQSRWNDKHSGQYSD6GWJTHT5N76HFZ84AKTN74EXHP7VDCAMYC5Q1EQHGKT67VYHVX1AZ6UT28TXZ6FWNNTXBGRX6NTXUGUCNNW3X6JT6NYRUJFR6T9XS4RWXKWXNA"
            }
        }

**NOTE**:

- `licenseCode` comes from saved value from most recent GET https://dev.services.32point6.com/api/v1/admin/iot/license/00-14-22-01-23-45

- `correlationRefId` comes from saved value from most recent GET https://dev.services.32point6.com/api/v1/admin/iot/license/00-14-22-01-23-45
