# SHOW_VIDEO = True

UPLOAD_FACE = False

DEBUG = True


SAMPLE_CONFIG = {
    u'correlationRefId': u'00000000-0000-0000-0000-000000000000',
    u'createdOn': u'2017-12-08T20:53:15.0960615+00:00',
    u'data': {
        u'beaconMajor': 1,
        u'beaconMinor': 2,
        u'beaconName': u'Lazlo',
        u'beaconUuid': u'd405405e-9402-4b56-b368-764fc3dd8489',
        u'geoFencePolygon': u'-84.3574221432209,34.51742011432837,-85.3242190182209,34.15454540561995,'
                            u'-85.3242190182209,33.38742104822809,-84.0937502682209,33.35072034573742,'
                            u'-83.3027346432209,34.0817822993925,-84.0058596432209,34.589806456800915,'
                            u'-84.35987117991726,34.515140941042354,-84.35742295189027,'
                            u'34.51741926347808,-84.3574221432209,34.51742011432837',
        u'iotConnectionString': u'HostName=devlng.azure-devices.net;'
                                u'DeviceId=729b7b07-93a2-4c4d-9e1c-69b850dfabcf;'
                                u'SharedAccessKey=RFj1XWj657rXhaXW4SC5xo2dlwKZRI3eewPna0yrpa8='
    },
    u'error': None
}

SAMPLE_LICENSE_CODE = {
    'correlationRefId': u'00000000-0000-0000-0000-000000000000',
    'licenseCode': u'00KtNBVhW0iAPr:A+EA^gIpVeoh{4es3*wuoMJ3NkkPM^OFHAYW0C!>BA9V})y1/q+t@FOOy4U8ERJSQkJ/)T.JhIhe0K%'
                   u'm0jk=a:)l#kCMZ:/2hke%fe{1&yoXmJ[kF4Oc?is:KksO/'
}
