import json
import requests
import time
from settings import *
from src.utils.common import get_mac_address


headers = {'Lazlo-AuthorityLicenseCode': AUTH_CODE, 'Content-Type': 'application/json'}


def get_config():
    """
    Get config information from the server.
    :return:
    """
    if DEBUG:
        time.sleep(1)
        return SAMPLE_CONFIG
    try:
        mac = get_mac_address()
        url = URL_BASE + URL_CONFIG + mac
        response = requests.get(url=url, headers=headers)
        json_data = response.json()
        if response.status_code == 200:
            return json_data
        else:
            logger.error('Failed to get config - {}'.format(json_data))
    except Exception as e:
        logger.error('Failed to get Azure Connection String - {}'.format(e))


def get_license_code():
    """
    Get licenseCode & correlationRefId from the server.
    :return:
    """
    if DEBUG:
        time.sleep(1)
        return SAMPLE_LICENSE_CODE
    try:
        mac = get_mac_address()
        url = URL_BASE + URL_LICENCE + mac
        response = requests.get(url=url, headers=headers)
        if response.status_code == 200:
            json_data = response.json()
            return {'correlationRefId': json_data['correlationRefId'],
                    'licenseCode': json_data['data']['licenseCode']
                    }
    except Exception as e:
        logger.error('Failed to get licenseCode & correlationRefId - {}'.format(e))


def checkout(data):
    """
    :param data:
        {
            "correlationRefId": "9378c5e9-cf78-401a-8405-620b2b989d5d",
            "uuid": "FFA0DF09-A98A-4F7E-8E69-80208807D96C",
            "latitude": 42.136268881127663,
            "longitude": -80.094999131507038,
            "createdOn": "2017-11-05T23:02:03.2845163+00:00",
            "data": {
                "licenseCode": "D4fiYCYj6mmmSpgclwiqtRmU3ns=:7DQSRWNDKHSGQYSD6GWJTHT5N76HFZ84AKTN74EXHP7VDCAMYC5Q1
                                EQHGKT67VYHVX1AZ6UT28TXZ6FWNNTXBGRX6NTXUGUCNNW3X6JT6NYRUJFR6T9XS4RWXKWXNA"
            }
        }
    :return:
    """
    if DEBUG:
        time.sleep(1)
        return True
    try:
        url = URL_BASE + URL_CHECKOUT
        payload = json.dumps(data, ensure_ascii=False)
        response = requests.put(url=url, data=payload, headers=headers)
        if response.status_code == 204:
            return True
    except Exception as e:
        logger.error('Failed to get licenseCode & correlationRefId - {}'.format(e))


if __name__ == '__main__':
    logger.info('Starting utility')
    import pprint
    pprint.pprint(get_config())
    pprint.pprint(get_license_code())
    # checkout(data={
    #     "correlationRefId": "9378c5e9-cf78-401a-8405-620b2b989d5d",
    #     "uuid": "FFA0DF09-A98A-4F7E-8E69-80208807D96C",
    #     "latitude": 42.136268881127663,
    #     "longitude": -80.094999131507038,
    #     "createdOn": "2017-11-05T23:02:03.2845163+00:00",
    #     "data": {
    #         "licenseCode": "D4fiYCYj6mmmSpgclwiqtRmU3ns=:7DQSRWNDKHSGQYSD6GWJTHT5N76HFZ84AKTN74EXHP7VDCAMYC5Q1"
    #                        "EQHGKT67VYHVX1AZ6UT28TXZ6FWNNTXBGRX6NTXUGUCNNW3X6JT6NYRUJFR6T9XS4RWXKWXNA"
    #     }
    # })
