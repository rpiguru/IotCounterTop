import barcode
from barcode.writer import *
from pyqrcode import QRCode, tables
from pyzbar.pyzbar import decode
from settings import logger


class MyBarcodeWriter(ImageWriter):

    def __init__(self, title):
        super(MyBarcodeWriter, self).__init__()
        # ========== Experimental Values ==========
        self.dpi = 150
        self.offset_x = 22
        self.offset_y = 20
        # =========================================
        self.title = title

    def _paint_text(self, xpos, ypos):
        """
        Re-define this function to calibrate the position of the barcode text
        :param xpos:
        :param ypos:
        :return:
        """
        font = ImageFont.truetype(FONT, self.font_size * 2)
        width, height = font.getsize(self.text)
        pos = (mm2px(xpos, self.dpi) - width // 2 + self.offset_x,
               mm2px(ypos, self.dpi) - height // 4 - self.offset_y)
        self._draw.text(pos, self.text, font=font, fill=self.foreground)
        if self.title:
            self._draw.rectangle(((0, 0), (self._image.size[0], self._image.size[1] // 4)), fill='white')
            font = ImageFont.truetype(FONT, self.font_size * 2)
            width, height = font.getsize(self.title)
            pos = self._image.size[0] // 2 - width // 2, 7
            self._draw.text(pos, self.title, font=font, fill=self.foreground)


def create_barcode_png(code='012000018800', title=None):
    upc = barcode.get_barcode_class('upc')
    inst_upc = upc(code, writer=MyBarcodeWriter(title=title), make_ean=True)
    fullname = inst_upc.save(code)
    return fullname


def decode_qr_code(img_path):
    result = decode(Image.open(img_path))
    if result:
        return result[0].data


def compose_qr_code_image(code):
    """
    Create QR Code image in 1 bit BMP format
    :param code:
    :return: path of bmp image file
    """
    qr = QRCode(code)
    scale = 300 // (tables.version_size[qr.version] + 1)
    qr.png('/tmp/qrcode.png', scale=scale, module_color=[0, 0, 0, 255])
    return '/tmp/qrcode.png'


def get_ap_info_from_barcode(img_path):
    """
    Decode the QR Code image generated on https://qifi.org
    Sample QR Code: WIFI:S:Test-AP;T:WPA;P:Test-password;;
    :param img_path: Path of the image
    :return: Dictionary
        {'ssid': '', 'password': ''}
    :rtype dict
    """
    code = decode_qr_code(img_path)
    ssid = _type = pwd = None
    if code:
        try:
            for block in [b for b in code.split(';') if b != '']:
                tmp = block.split(':')
                if tmp[0] == 'WIFI':
                    ssid = tmp[2]
                elif tmp[0] == 'T':
                    _type = tmp[1]
                elif tmp[0] == 'P':
                    pwd = tmp[1]
            return {'ssid': ssid, 'type': _type, 'password': pwd}
        except Exception as e:
            logger.error('Failed to decode QR Code({}) - {}'.format(code, e))


if __name__ == '__main__':

    # print create_barcode_png()
    # print decode_qr_code(os.path.expanduser('~/Downloads/Test-AP-qrcode.png'))
    # create_barcode_png()
    compose_qr_code_image('D4fiYCYj6mmmSpgclwiqtRmU3ns=:7DQSRWNDKHSGQYSD6GWJTHT5N76HFZ84AKTN74EXHP7VDCAMYC5Q1EQHGK'
                          'T67VYHVX1AZ6UT28TXZ6FWNNTXBGRX6NTXUGUCNNW3X6JT6NYRUJFR6T9XS4RWXKWXNA')
