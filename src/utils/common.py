"""
    Common utilities
"""
import os
import subprocess
import netifaces
import time
from settings import logger, DEBUG
import httplib
import socket


def is_rpi():
    return 'arm' in os.uname()[4]


def get_revision():
    """
    Check RPi 3 or zero.
    Refer: https://raspberrypi.stackexchange.com/questions/61699/is-there-a-bash-command-to-check-if-the-raspberry-pi-is-2-or-3/61701
    :return: 3 for RPi 3, and 0 for Zero
    """
    with open('/proc/cpuinfo') as lines:
        for line in lines:
            if line.startswith('Revision'):
                revision = line[line.index(':') + 1:].strip()
                if revision in ['a02082', 'a22082']:
                    return 3
                elif revision in ['0x9000C1', '90092', '90093']:
                    return 0


def get_current_wifi_info():
    """
    Get the detailed information of the wifi AP.
    :return:
    """
    if is_rpi():
        pipe = os.popen('iwgetid -r')
        ap = pipe.read().strip()
        pipe.close()
        if ap == '':
            return None, None
        else:
            try:
                p = subprocess.Popen('iwconfig | grep "Access Point"', shell=True, stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                output, error = p.communicate()
                p.wait()
                line = output.decode('utf-8').splitlines()[-1].strip()
                mac = line.split('Access Point: ')[1].split()[0]
                return ap, mac
            except (ValueError, IndexError, ArithmeticError, TypeError):
                return None, None
    ap = 'Testing AP'
    mac = '11:22:33:44:55:66'
    return ap, mac


def check_internet_connection(url='www.google.com', timeout=5):
    """
    Check internet connection
        It will be faster to just make a HEAD request so no HTML will be fetched.
        Also I am sure google would like it better this way :)
    """
    conn = httplib.HTTPConnection(url, timeout=timeout)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except socket.error:
        conn.close()
        return False


def get_mac_address(interface='wlan0'):
    """
    Retrieve MAC address of the wifi interface
    :return:
    """
    if is_rpi():
        try:
            info = netifaces.ifaddresses(interface)[netifaces.AF_LINK]
            return info[0]['addr'].replace(':', '-')
        except Exception as e:
            if not DEBUG:
                logger.error('Failed to get MAC address of {} - {}'.format(interface, e))
                return
    return '00-14-22-01-23-45'


def connect_to_ap(ssid='', pwd=''):
    """
    Connect to AP and return new assigned IP address
    :param ssid:
    :param pwd:
    :return: Return None when failed, otherwise return (IP, is_restored)
    """
    # Disconnect from the current AP:
    ap, mac = get_current_wifi_info()
    if ap:
        print('Disconnecting {}...'.format(ap))
        p = subprocess.Popen('sudo nmcli d disconnect wlan0', shell=True, preexec_fn=os.setpgrp,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        print('Result => Out: {}, Error: {}'.format(stdout, stderr))
        p.wait()

    time.sleep(.5)

    logger.info('Net: Connecting to {}...'.format(ssid))
    p = subprocess.Popen('sudo nmcli dev wifi connect "{}" password "{}"'.format(ssid, pwd),
                         shell=True, preexec_fn=os.setpgrp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    logger.info('Net: Result => Out: {}, Error: {}'.format(stdout, stderr))
    p.wait()

    return get_ip_address('wlan0')


def get_ip_address(ifname='wlan0'):
    """
    Get assigned IP address of given interface
    :param ifname: interface name such as wlan0, eth0, etc
    :return: If not on RPi, returns the IP address of LAN
    """
    if not is_rpi():
        ifname = 'enp3s0'
    try:
        return netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    except Exception as e:
        logger.error('Net: Failed to get IP address of {}, reason: {}'.format(ifname, e))


if __name__ == '__main__':
    print check_internet_connection(url='www.google.com')
