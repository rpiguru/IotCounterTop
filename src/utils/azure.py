"""
    Upload files to Azure IoT Hub
    Refer: https://github.com/jonbgallant/azure-iot-rest

"""
import requests
from base64 import b64encode, b64decode
from hashlib import sha256
import time
from urllib import quote_plus, urlencode
from hmac import HMAC
from settings import logger


tokenExpirationPeriod = 60
policyKeyName = 'device'
api_ver = '2016-02-03'


def _get_iot_hub_sas_token(uri, _key, policy_name, expiry=3600):
    ttl = time.time() + expiry
    sign_key = "%s\n%d" % ((quote_plus(uri)), int(ttl))
    signature = b64encode(HMAC(b64decode(_key), sign_key, sha256).digest())
    raw_token = {
        'sr': uri,
        'sig': signature,
        'se': str(int(ttl))
    }
    if policy_name is not None:
        raw_token['skn'] = policy_name
    return 'SharedAccessSignature {}'.format(urlencode(raw_token))


def _parse_connection_string(conn_str=''):
    try:
        data = {}
        for t in conn_str.split(';'):
            key_val = t.split('=')
            data[key_val[0]] = t[len(key_val[0]) + 1:]
        # FIXME: Remove this line!
        data['SharedAccessKey'] = 'FWmva0xM1rcF5aLJBMZPmrIFm36J8LuoG7BVSRkac8I='
        return data
    except Exception as e:
        logger.error('Connection String: Invalid({}) - {}'.format(conn_str, e))


def upload_file_to_azure_iot_hub(file_path, file_name, content_type='image/png', connection_string=''):
    metadata = _parse_connection_string(connection_string)
    if metadata:
        try:
            sas_token = _get_iot_hub_sas_token(uri=metadata['HostName'],
                                               _key=metadata['SharedAccessKey'],
                                               policy_name=policyKeyName, expiry=tokenExpirationPeriod)
            # 1. Get file storage uri
            _uri = 'https://%s/devices/%s/files?api-version=%s' % (metadata['HostName'], metadata['DeviceId'], api_ver)
            file_storage_resp = requests.post(_uri,
                                              headers={'Authorization': sas_token, 'Content-Type': 'application/json'},
                                              data='{ "blobName": "%s"}' % file_name)
            if file_storage_resp.status_code == 200:
                f_parts = file_storage_resp.json()
                # 2. Upload file to Blob Storage
                _uri = 'https://%s/%s/%s%s' % (
                    f_parts['hostName'], f_parts['containerName'], f_parts['blobName'], f_parts['sasToken'])
                content = open(file_path, 'rb').read()
                upload_resp = requests.put(_uri,
                                           headers={
                                               'Content-Type': content_type,
                                               'Content-Length': str(len(content)),
                                               'x-ms-blob-type': 'BlockBlob',
                                           },
                                           data=content)
                if upload_resp.status_code == 201:
                    # 3. GET UPLOAD FILE NOTIFICATION
                    _uri = 'https://%s/devices/%s/files/notifications?api-version=%s' % (
                        metadata['HostName'], metadata['DeviceId'], api_ver)
                    notif_resp = requests.post(_uri,
                                               headers={'Authorization': sas_token, 'Content-Type': 'application/json'},
                                               data='{"correlationId": "%s" }' % (f_parts["correlationId"]))
                    logger.info('File Upload: Status code: {}'.format(notif_resp.status_code))
                    return notif_resp.status_code == 204
                else:
                    logger.error('Failed to upload file - {} - {}'.format(upload_resp.status_code, upload_resp.text))
            else:
                logger.error('Failed to get File Storage URI - {}'.format(file_storage_resp.text))
        except Exception as e:
            logger.exception('File Upload: Unexpected error - {}'.format(e))


def upload_image_to_azure_iot_hub(img_path, file_name, connection_string):
    return upload_file_to_azure_iot_hub(file_path=img_path, file_name=file_name,
                                        content_type='image/{}'.format(file_name.split('.')[1]),
                                        connection_string=connection_string)


if __name__ == '__main__':
    print(upload_image_to_azure_iot_hub(img_path='../assets/clerk_logo.jpg', file_name='clerk_logo.jpg',
                                        connection_string='HostName=devlng.azure-devices.net;'
                                                          'DeviceId=00-14-22-01-23-45;SharedAccessKey='
                                                          'FWmva0xM1rcF5aLJBMZPmrIFm36J8LuoG7BVSRkac8I='))
