import re
import subprocess


def _hexify(i, digits=2):
    # convert an integer into a hex value of a given number of digits
    format_string = "0%dx" % digits
    return format(i, format_string).upper()


def start_beacon(uuid='', major=1, minor=2):
    """
    Start iBeacon
    :param major:
    :param minor:
    :param uuid:
    :return:
    """
    identifier = '05 99'        # Bluetooth Company ID of Lazlo
    ibeacon_prefix = "1E 02 01 1A 1A FF {} 02 15".format(identifier)

    uuid_list = re.findall('..', uuid.replace('-', '').upper())
    major_hex = re.findall('..', _hexify(major, 4))
    minor_hex = re.findall('..', _hexify(minor, 4))
    data = ' '.join(uuid_list + major_hex + minor_hex)

    cmd_list = [
        "sudo hciconfig hci0 up",               # initialize device
        "sudo hciconfig hci0 noleadv",          # disable advertising
        "sudo hciconfig hci0 noscan",           # stop the dongle looking for other Bluetooth devices
        "sudo hciconfig hci0 pscan",
        "sudo hciconfig hci0 leadv",
        "sudo hcitool -i hci0 cmd 0x08 0x0008 {} {} C8 00".format(ibeacon_prefix, data),
        "sudo hcitool -i hci0 cmd 0x08 0x0006 A0 00 A0 00 00 00 00 00 00 00 00 00 00 07 00",
        "sudo hcitool -i hci0 cmd 0x08 0x000a 01"
    ]
    for cmd in cmd_list:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        p.wait()
        print '    Out => ', stdout, ' Err => ', stderr


if __name__ == '__main__':
    start_beacon('d405405e-9402-4b56-b368-764fc3dd8489', major=0, minor=1)
