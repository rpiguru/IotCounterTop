import threading
import serial
import pynmea2
import time
import cv2
import copy
import numpy
from pynmea2 import ChecksumError, ParseError, SentenceTypeError


def parse_gps(data):
    if data.find('GGA') > 0:
        try:
            msg = pynmea2.parse(data)
            # print "Timestamp: %s -- Lat: %s %s -- Lon: %s %s -- Altitude: %s %s" % (
            #     msg.timestamp, msg.lat, msg.lat_dir, msg.lon, msg.lon_dir, msg.altitude, msg.altitude_units)
            if msg.lat != '':
                return msg
        except (ChecksumError, ParseError, SentenceTypeError):
            pass


class GPSService(threading.Thread):

    lock = threading.RLock()

    lat = 0
    lon = 0

    def __init__(self, port='/dev/ttyS0', baudrate=9600, timeout=.5):
        super(GPSService, self).__init__()
        self.ser = serial.Serial(port=port, baudrate=baudrate, timeout=timeout)

    def run(self):
        while True:
            line_data = self.ser.readline()
            msg = parse_gps(line_data)
            if msg:
                with self.lock:
                    self.lat = float(msg.lat) / 100
                    self.lon = float(msg.lon) / 100
            time.sleep(2)

    def validate(self, polygon_list):
        """
        Validate if (lat, lon) is inside of the polygon
        :param polygon_list: [x1, y1, x2, y2, x3, y3, ...]
        :return:
        """
        if not polygon_list:
            return True
        with self.lock:
            lat = copy.deepcopy(self.lat)
            lon = copy.deepcopy(self.lon)
        if lat != 0 and lon != 0:
            points = [[x, y] for x, y in zip(polygon_list[::2], polygon_list[1::2])]
            contour = numpy.array(points, dtype=numpy.int32)
            return cv2.pointPolygonTest(contour, (lat, lon), False) > 0
        else:       # Not received any data from GPS module yet?
            return True

    def get_position(self):
        with self.lock:
            lat = copy.deepcopy(self.lat)
            lon = copy.deepcopy(self.lon)
        return lat, lon


if __name__ == '__main__':

    s = GPSService(port='/dev/ttyUSB0')
    s.lat = 50
    s.lon = 50

    polygons = [30, 30, 60, 30, 60, 60, 30, 60, 45, 15]

    print s.validate(polygons)
