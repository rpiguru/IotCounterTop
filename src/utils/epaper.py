"""
    Utility for e-paper
"""
from PIL import Image, ImageDraw, ImageFont

from settings import *
from src.epd.epd2in9 import EPD2In9
from src.epd.epd2in9b import EPD2In9B
from src.epd.epd4in2b import EPD4In2B
from src.utils.barcode_util import create_barcode_png, compose_qr_code_image
from src.utils.common import is_rpi


def _paste_image(image, size=(400, 300), offset=(0, 0)):
    image_template = Image.new('1', size, 255)  # 255: clear the frame
    image_mono_color = image.convert('1')
    image_template.paste(image_mono_color, offset)
    return image_template


def _add_wifi_icon(img):
    img.paste(Image.open('src/assets/wifi_on.bmp'), (270, 3))
    return img


class EPD4Service:

    def __init__(self):
        self.epd_4in2b = EPD4In2B(spi_channel=0, rst_pin=EPD4in2B_RST_PIN, dc_pin=EPD4in2B_DC_PIN,
                                  busy_pin=EPD4in2B_BUSY_PIN)

    def display_qr_code(self, code):
        """
        Display QR Code on the large e-Paper.
        :param code:
        :return:
        """
        logger.debug('EPD4: Displaying QR Code({})'.format(code))
        png_file = compose_qr_code_image(code)
        _img = _paste_image(Image.open(png_file), (self.epd_4in2b.width, self.epd_4in2b.height), (52, 0))
        self.epd_4in2b.display_image(_img)

    def display_clerk_logo(self):
        """
        Show Clerk logo on the large e-paper
        :return:
        """
        logger.debug('EPD4: Displaying Clerk logo')
        self.epd_4in2b.display_image(Image.open('src/assets/clerk_logo.jpg'))

    def clear(self):
        """
        Clear the large e-paper
        :return:
        """
        logger.debug('EPD4: Clear')
        self.epd_4in2b.clear()

    def is_busy(self):
        return self.epd_4in2b.is_busy()


class EPD2Service:

    def __init__(self):
        if EPD2in9_TYPE == '2in9':
            self.epd = EPD2In9(spi_channel=1, rst_pin=EPD2in9B_RST_PIN, dc_pin=EPD2in9B_DC_PIN,
                               busy_pin=EPD2in9B_BUSY_PIN, rotate=EPD2in9B_ROTATION)
            self.epd.start_full_drawing()
        else:
            self.epd = EPD2In9B(spi_channel=1, rst_pin=EPD2in9B_RST_PIN, dc_pin=EPD2in9B_DC_PIN,
                                busy_pin=EPD2in9B_BUSY_PIN, rotate=EPD2in9B_ROTATION)

    def display_upc12_code(self, code, amount_due=None, wifi_icon=False):
        """
        Create a barcode image(UPC-A) and display it on the e-Paper.
        :param wifi_icon: If True, add `WiFi` icon at top-left corner
        :param code:
        :param amount_due:
        :return:
        """
        png_file = create_barcode_png(code=code, title='Amount Due: {}'.format(amount_due))
        _img = _paste_image(Image.open(png_file), (self.epd.height, self.epd.width), (18, 0))
        if wifi_icon:
            _img = _add_wifi_icon(_img)
        self.epd.display_image(_img)

    def clear(self):
        """
        Clear the small e-paper
        :return:
        """
        logger.debug('EPD2: Clear')
        self.epd.clear()

    def display_lazlo_logo(self):
        """
        Show Lazlo logo on the small e-paper
        :return:
        """
        logger.debug('EPD2: Displaying Lazlo Logo')
        self.epd.display_image(Image.open('src/assets/lazlo_logo.jpg'))

    def display_text(self, contents, wifi_icon=False):
        """
        Display text sentences
        :param contents: List of sentences
            Example value:
                [
                    {'text': "Oops! You're not in", 'font_size': 23, 'left': 20, 'top': 30},
                    {'text': "a valid location!", 'font_size': 23, 'left': 20, 'top': 70},
                ]
        :param wifi_icon: If True, add `WiFi` icon at top-left corner
        :return:
        """
        logger.debug('EPD2: Display text - {}'.format('\n'.join([c['text'] for c in contents])))
        width, height = self.epd.height, self.epd.width
        img = Image.new('RGB', (width, height), (255, 255, 255))
        draw = ImageDraw.Draw(img)
        lines = len(contents)
        for i, line in enumerate(contents):
            _font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf', size=line['font_size'])
            draw.text(xy=(line.get('left', 10), line.get('top', height // lines // 1.5 * (i + 1))),
                      text=line['text'], font=_font, fill='black')
        if wifi_icon:
            img = _add_wifi_icon(img)
        if is_rpi():
            self.epd.display_image(img)
        else:
            img.show()

    def is_busy(self):
        return self.epd.is_busy()


if __name__ == '__main__':

    # print create_barcode_png()
    epd2 = EPD2Service()
    epd2.display_upc12_code('123456789012')

    # display_qr_code('Here is the sample code')

    # img = Image.open(_compose_qr_code_image('Here is the sample code', 8))
    # img.show()

    # print _img_to_bmp('../assets/wifi_off.jpg', remove_src=False)
