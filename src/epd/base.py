import spidev
import time
import os

try:
    import RPi.GPIO as GPIO
except ImportError:
    pass


def is_rpi():
    return 'arm' in os.uname()[4]


if is_rpi():
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)


class EPDBase:
    """
    Basic class for e-Papers
    NOTE: BUSY_STATE is `1` for 2-color e-Papers
    """
    _busy = False

    def __init__(self, spi_channel=0, width=400, height=300, rst_pin=17, dc_pin=27, busy_pin=22, busy_state=1):
        self.width = width
        self.height = height
        self.rst_pin = rst_pin
        self.dc_pin = dc_pin
        self.busy_pin = busy_pin
        self.busy_state = busy_state         # 1 for EPD, 0 for EPD-B
        if is_rpi():
            self.spi = spidev.SpiDev(spi_channel, 0)
            GPIO.setup(self.rst_pin, GPIO.OUT)
            GPIO.setup(self.dc_pin, GPIO.OUT)
            GPIO.setup(self.busy_pin, GPIO.IN)
            self.spi.max_speed_hz = 2000000
            self.spi.mode = 0b00
            self.reset()

    def reset(self):
        if is_rpi():
            GPIO.output(self.rst_pin, GPIO.LOW)  # module reset
            time.sleep(.2)
            GPIO.output(self.rst_pin, GPIO.HIGH)
            time.sleep(.2)

    def send_command(self, command):
        if is_rpi():
            GPIO.output(self.dc_pin, GPIO.LOW)
            self.spi.writebytes([command])

    def send_data(self, data):
        if is_rpi():
            GPIO.output(self.dc_pin, GPIO.HIGH)
            self.spi.writebytes([data])

    def wait_until_idle(self):
        if is_rpi():
            while GPIO.input(self.busy_pin) == self.busy_state:
                time.sleep(.1)

    def is_busy(self):
        return self._busy


#  === Common commands ===
DATA_START_TRANSMISSION_1           = 0x10
DISPLAY_REFRESH                     = 0x12
DATA_START_TRANSMISSION_2           = 0x13


class EPDBaseB(EPDBase):
    """
    Basic class for Three Color e-Papers.
    NOTE: BUSY_STATE is `0` for three color e-Papers
    """

    def __init__(self, spi_channel=0, width=400, height=300, rst_pin=17, dc_pin=27, busy_pin=22, rotate=0):
        EPDBase.__init__(self, spi_channel=spi_channel, width=width, height=height, rst_pin=rst_pin, dc_pin=dc_pin,
                         busy_pin=busy_pin, busy_state=0)
        self.rotate = rotate

    def get_frame_buffer(self, image):
        buf = [0xFF] * (self.width * self.height / 8)
        # Set buffer to value of Python Imaging Library image.
        # Image must be in mode 1.
        image_mono_color = image.convert('1')
        im_width, im_height = image_mono_color.size
        if self.rotate in [0, 2]:
            if im_width != self.width or im_height != self.height:
                raise ValueError('Image must be same dimensions as display ({0}x{1}).'.format(self.width, self.height))
        else:
            if im_width != self.height or im_height != self.width:
                raise ValueError('Image must be same dimensions as display ({0}x{1}).'.format(self.width, self.height))

        pixels = image_mono_color.load()
        for y in range(self.height):
            for x in range(self.width):
                # Set the bits for the column of pixels at the current position.
                if self.rotate == 0:
                    if pixels[x, y] == 0:
                        buf[(x + y * self.width) / 8] &= ~(0x80 >> (x % 8))
                elif self.rotate == 1:
                    if pixels[y, x] == 0:
                        buf[(x + (self.height - y) * self.width) / 8] &= ~(0x80 >> (x % 8))
                # TODO: Implement when rotation = 2 or 3

        return buf

    def display_image(self, image, color='black'):
        if color == 'black':
            self.display_frame(frame_buffer_black=self.get_frame_buffer(image))
        else:
            self.display_frame(frame_buffer_red=self.get_frame_buffer(image))

    def display_frame(self, frame_buffer_black=None, frame_buffer_red=None, clear=False):
        """
        Display image frame on the screen
        :param frame_buffer_black:
        :param frame_buffer_red:
        :param clear: If True, clear unused color
        :return:
        """
        self._busy = True
        frame_clear = [0xFF] * (self.width * self.height / 8)
        if frame_buffer_black is not None:
            self._transfer_frame_data(frame_buffer_black, DATA_START_TRANSMISSION_1)
        elif clear:
            self._transfer_frame_data(frame_clear, DATA_START_TRANSMISSION_1)

        if frame_buffer_red is not None:
            self._transfer_frame_data(frame_buffer_red, DATA_START_TRANSMISSION_2)
        elif clear:
            self._transfer_frame_data(frame_clear, DATA_START_TRANSMISSION_2)

        self.send_command(DISPLAY_REFRESH)
        self.wait_until_idle()
        self._busy = False

    def clear(self):
        self.display_frame(clear=True)

    def _transfer_frame_data(self, frame, address):
        self.send_command(address)
        time.sleep(.002)
        for i in range(0, self.width * self.height / 8):
            self.send_data(frame[i])
        time.sleep(.002)
