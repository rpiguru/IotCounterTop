from PIL import Image
from PIL import ImageDraw
from base import EPDBaseB

# Display resolution
WIDTH = 128
HEIGHT = 296

# EPD2IN9B commands
PANEL_SETTING                       = 0x00
POWER_SETTING                       = 0x01
POWER_OFF                           = 0x02
POWER_OFF_SEQUENCE_SETTING          = 0x03
POWER_ON                            = 0x04
POWER_ON_MEASURE                    = 0x05
BOOSTER_SOFT_START                  = 0x06
DEEP_SLEEP                          = 0x07
DATA_STOP                           = 0x11
PLL_CONTROL                         = 0x30
TEMPERATURE_SENSOR_COMMAND          = 0x40
TEMPERATURE_SENSOR_CALIBRATION      = 0x41
TEMPERATURE_SENSOR_WRITE            = 0x42
TEMPERATURE_SENSOR_READ             = 0x43
VCOM_AND_DATA_INTERVAL_SETTING      = 0x50
LOW_POWER_DETECTION                 = 0x51
TCON_SETTING                        = 0x60
TCON_RESOLUTION                     = 0x61
GET_STATUS                          = 0x71
AUTO_MEASURE_VCOM                   = 0x80
VCOM_VALUE                          = 0x81
VCM_DC_SETTING_REGISTER             = 0x82
PARTIAL_WINDOW                      = 0x90
PARTIAL_IN                          = 0x91
PARTIAL_OUT                         = 0x92
PROGRAM_MODE                        = 0xA0
ACTIVE_PROGRAM                      = 0xA1
READ_OTP_DATA                       = 0xA2
POWER_SAVING                        = 0xE3

# Display orientation
ROTATE_0 = 0
ROTATE_90 = 1
ROTATE_180 = 2
ROTATE_270 = 3


# Pin definition
RST_PIN = 5
DC_PIN = 6
BUSY_PIN = 13


class EPD2In9B(EPDBaseB):

    spi = None

    def __init__(self, spi_channel=1, width=WIDTH, height=HEIGHT, rotate=1,
                 rst_pin=RST_PIN, dc_pin=DC_PIN, busy_pin=BUSY_PIN):
        EPDBaseB.__init__(self, spi_channel=spi_channel, width=width, height=height, rotate=rotate,
                          rst_pin=rst_pin, dc_pin=dc_pin, busy_pin=busy_pin)
        self.send_command(BOOSTER_SOFT_START)
        self.send_data(0x17)
        self.send_data(0x17)
        self.send_data(0x17)
        self.send_command(POWER_ON)
        self.wait_until_idle()
        self.send_command(PANEL_SETTING)
        self.send_data(0x8F)
        self.send_command(VCOM_AND_DATA_INTERVAL_SETTING)
        self.send_data(0x77)
        self.send_command(TCON_RESOLUTION)
        self.send_data(0x80)
        self.send_data(0x01)
        self.send_data(0x28)
        self.send_command(VCM_DC_SETTING_REGISTER)
        self.send_data(0x0A)

    # after this, call epd.init() to awaken the module
    def sleep(self):
        self.send_command(VCOM_AND_DATA_INTERVAL_SETTING)
        self.send_data(0x37)
        self.send_command(VCM_DC_SETTING_REGISTER)  # to solve Vcom drop
        self.send_data(0x00)
        self.send_command(POWER_SETTING)  # power setting
        self.send_data(0x02)  # gate switch to external
        self.send_data(0x00)
        self.send_data(0x00)
        self.send_data(0x00)
        self.wait_until_idle()
        self.send_command(POWER_OFF)  # power off

    def set_rotate(self, rotate):
        if rotate == ROTATE_0:
            self.rotate = ROTATE_0
            self.width = WIDTH
            self.height = HEIGHT
        elif rotate == ROTATE_90:
            self.rotate = ROTATE_90
            self.width = HEIGHT
            self.height = WIDTH
        elif rotate == ROTATE_180:
            self.rotate = ROTATE_180
            self.width = WIDTH
            self.height = HEIGHT
        elif ROTATE_270 == rotate:
            self.rotate = ROTATE_270
            self.width = HEIGHT
            self.height = WIDTH

    def set_pixel(self, frame_buffer, x, y, colored):
        if x < 0 or x >= WIDTH or y < 0 or y >= HEIGHT:
            return
        if self.rotate == ROTATE_0:
            self.set_absolute_pixel(frame_buffer, x, y, colored)
        elif self.rotate == ROTATE_90:
            point_temp = x
            x = WIDTH - y
            y = point_temp
            self.set_absolute_pixel(frame_buffer, x, y, colored)
        elif self.rotate == ROTATE_180:
            x = WIDTH - x
            y = HEIGHT - y
            self.set_absolute_pixel(frame_buffer, x, y, colored)
        elif self.rotate == ROTATE_270:
            point_temp = x
            x = y
            y = HEIGHT - point_temp
            self.set_absolute_pixel(frame_buffer, x, y, colored)

    @staticmethod
    def set_absolute_pixel(frame_buffer, x, y, colored):
        # To avoid display orientation effects
        # use EPD_WIDTH instead of self.width
        # use EPD_HEIGHT instead of self.height
        if x < 0 or x >= WIDTH or y < 0 or y >= HEIGHT:
            return
        if colored:
            frame_buffer[(x + y * WIDTH) / 8] &= ~(0x80 >> (x % 8))
        else:
            frame_buffer[(x + y * WIDTH) / 8] |= 0x80 >> (x % 8)

    def draw_string_at(self, frame_buffer, x, y, text, font, colored):
        image = Image.new('1', (self.width, self.height))
        draw = ImageDraw.Draw(image)
        draw.text((x, y), text, font=font, fill=255)
        # Set buffer to value of Python Imaging Library image.
        # Image must be in mode 1.
        pixels = image.load()
        for y in range(self.height):
            for x in range(self.width):
                # Set the bits for the column of pixels at the current position.
                if pixels[x, y] != 0:
                    self.set_pixel(frame_buffer, x, y, colored)

    def draw_line(self, frame_buffer, x0, y0, x1, y1, colored):
        # Bresenham algorithm
        dx = abs(x1 - x0)
        sx = 1 if x0 < x1 else -1
        dy = -abs(y1 - y0)
        sy = 1 if y0 < y1 else -1
        err = dx + dy
        while (x0 != x1) and (y0 != y1):
            self.set_pixel(frame_buffer, x0, y0, colored)
            if 2 * err >= dy:
                err += dy
                x0 += sx
            if 2 * err <= dx:
                err += dx
                y0 += sy

    def draw_horizontal_line(self, frame_buffer, x, y, width, colored):
        for i in range(x, x + width):
            self.set_pixel(frame_buffer, i, y, colored)

    def draw_vertical_line(self, frame_buffer, x, y, height, colored):
        for i in range(y, y + height):
            self.set_pixel(frame_buffer, x, i, colored)

    def draw_rectangle(self, frame_buffer, x0, y0, x1, y1, colored):
        min_x = x0 if x1 > x0 else x1
        max_x = x1 if x1 > x0 else x0
        min_y = y0 if y1 > y0 else y1
        max_y = y1 if y1 > y0 else y0
        self.draw_horizontal_line(frame_buffer, min_x, min_y, max_x - min_x + 1, colored)
        self.draw_horizontal_line(frame_buffer, min_x, max_y, max_x - min_x + 1, colored)
        self.draw_vertical_line(frame_buffer, min_x, min_y, max_y - min_y + 1, colored)
        self.draw_vertical_line(frame_buffer, max_x, min_y, max_y - min_y + 1, colored)

    def draw_filled_rectangle(self, frame_buffer, x0, y0, x1, y1, colored):
        min_x = x0 if x1 > x0 else x1
        max_x = x1 if x1 > x0 else x0
        min_y = y0 if y1 > y0 else y1
        max_y = y1 if y1 > y0 else y0
        for i in range(min_x, max_x + 1):
            self.draw_vertical_line(frame_buffer, i, min_y, max_y - min_y + 1, colored)

    def draw_circle(self, frame_buffer, x, y, radius, colored):
        # Bresenham algorithm
        x_pos = -radius
        y_pos = 0
        err = 2 - 2 * radius
        if x >= self.width or y >= self.height:
            return
        while True:
            self.set_pixel(frame_buffer, x - x_pos, y + y_pos, colored)
            self.set_pixel(frame_buffer, x + x_pos, y + y_pos, colored)
            self.set_pixel(frame_buffer, x + x_pos, y - y_pos, colored)
            self.set_pixel(frame_buffer, x - x_pos, y - y_pos, colored)
            e2 = err
            if e2 <= y_pos:
                y_pos += 1
                err += y_pos * 2 + 1
                if -x_pos == y_pos and e2 <= x_pos:
                    e2 = 0
            if e2 > x_pos:
                x_pos += 1
                err += x_pos * 2 + 1
            if x_pos > 0:
                break

    def draw_filled_circle(self, frame_buffer, x, y, radius, colored):
        # Bresenham algorithm
        x_pos = -radius
        y_pos = 0
        err = 2 - 2 * radius
        if x >= self.width or y >= self.height:
            return
        while True:
            self.set_pixel(frame_buffer, x - x_pos, y + y_pos, colored)
            self.set_pixel(frame_buffer, x + x_pos, y + y_pos, colored)
            self.set_pixel(frame_buffer, x + x_pos, y - y_pos, colored)
            self.set_pixel(frame_buffer, x - x_pos, y - y_pos, colored)
            self.draw_horizontal_line(frame_buffer, x + x_pos, y + y_pos, 2 * (-x_pos) + 1, colored)
            self.draw_horizontal_line(frame_buffer, x + x_pos, y - y_pos, 2 * (-x_pos) + 1, colored)
            e2 = err
            if e2 <= y_pos:
                y_pos += 1
                err += y_pos * 2 + 1
                if -x_pos == y_pos and e2 <= x_pos:
                    e2 = 0
            if e2 > x_pos:
                x_pos += 1
                err += x_pos * 2 + 1
            if x_pos > 0:
                break


if __name__ == '__main__':

    from PIL import ImageFont

    COLORED = 1
    UNCOLORED = 0

    epd = EPD2In9B()

    # clear the frame buffer
    frame_black = [0xFF] * (epd.width * epd.height / 8)
    frame_red = [0xFF] * (epd.width * epd.height / 8)

    # For simplicity, the arguments are explicit numerical coordinates
    epd.draw_rectangle(frame_black, 10, 80, 50, 140, COLORED)
    epd.draw_line(frame_black, 10, 80, 50, 140, COLORED)
    epd.draw_line(frame_black, 50, 80, 10, 140, COLORED)
    epd.draw_circle(frame_black, 90, 110, 30, COLORED)
    epd.draw_filled_rectangle(frame_red, 10, 180, 50, 240, COLORED)
    epd.draw_filled_rectangle(frame_red, 0, 6, 128, 26, COLORED)
    epd.draw_filled_circle(frame_red, 90, 210, 30, COLORED)

    # write strings to the buffer
    _font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMono.ttf', 16)
    epd.draw_string_at(frame_black, 4, 30, "e-Paper Demo", _font, COLORED)
    epd.draw_string_at(frame_red, 6, 10, "Hello world!", _font, UNCOLORED)
    # display the frames
    epd.display_frame(frame_black, frame_red)

    # display images
    # frame_black = epd.get_frame_buffer(Image.open('black.bmp'))
    # frame_red = epd.get_frame_buffer(Image.open('red.bmp'))
    # epd.display_frame(frame_black, frame_red)

    # You can get frame buffer from an image or import the buffer directly:
    # epd.display_frame(imagedata.IMAGE_BLACK, imagedata.IMAGE_RED)
