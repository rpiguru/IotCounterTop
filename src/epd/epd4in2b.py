from base import EPDBaseB

# Display resolution
WIDTH       = 400
HEIGHT      = 300

# EPD4IN2B commands
PANEL_SETTING                               = 0x00
POWER_SETTING                               = 0x01
POWER_OFF                                   = 0x02
POWER_OFF_SEQUENCE_SETTING                  = 0x03
POWER_ON                                    = 0x04
POWER_ON_MEASURE                            = 0x05
BOOSTER_SOFT_START                          = 0x06
DEEP_SLEEP                                  = 0x07
DATA_STOP                                   = 0x11
VCOM_LUT                                    = 0x20
W2W_LUT                                     = 0x21
B2W_LUT                                     = 0x22
W2B_LUT                                     = 0x23
B2B_LUT                                     = 0x24
PLL_CONTROL                                 = 0x30
TEMPERATURE_SENSOR_CALIBRATION              = 0x40
TEMPERATURE_SENSOR_SELECTION                = 0x41
TEMPERATURE_SENSOR_WRITE                    = 0x42
TEMPERATURE_SENSOR_READ                     = 0x43
VCOM_AND_DATA_INTERVAL_SETTING              = 0x50
LOW_POWER_DETECTION                         = 0x51
TCON_SETTING                                = 0x60
RESOLUTION_SETTING                          = 0x61
GSST_SETTING                                = 0x65
GET_STATUS                                  = 0x71
AUTO_MEASURE_VCOM                           = 0x80
VCOM_VALUE                                  = 0x81
VCM_DC_SETTING                              = 0x82
PARTIAL_WINDOW                              = 0x90
PARTIAL_IN                                  = 0x91
PARTIAL_OUT                                 = 0x92
PROGRAM_MODE                                = 0xA0
ACTIVE_PROGRAM                              = 0xA1
READ_OTP_DATA                               = 0xA2
POWER_SAVING                                = 0xE3


# Pin definition
RST_PIN = 17
DC_PIN = 27
BUSY_PIN = 22


class EPD4In2B(EPDBaseB):
    def __init__(self, spi_channel=0, width=WIDTH, height=HEIGHT,
                 rst_pin=RST_PIN, dc_pin=DC_PIN, busy_pin=BUSY_PIN):
        EPDBaseB.__init__(self, spi_channel=spi_channel, width=width,
                          height=height, rst_pin=rst_pin, dc_pin=dc_pin, busy_pin=busy_pin)
        self.send_command(BOOSTER_SOFT_START)
        self.send_data(0x17)
        self.send_data(0x17)
        self.send_data(0x17)  # 07 0f 17 1f 27 2F 37 2f
        self.send_command(POWER_ON)
        self.wait_until_idle()
        self.send_command(PANEL_SETTING)
        self.send_data(0x0F)  # LUT from OTP

    def sleep(self):
        self.send_command(VCOM_AND_DATA_INTERVAL_SETTING)
        self.send_data(0xF7)        # border floating
        self.send_command(POWER_OFF)
        self.wait_until_idle()
        self.send_command(DEEP_SLEEP)
        self.send_data(0xA5)        # check code


if __name__ == '__main__':
    from PIL import Image
    from PIL import ImageDraw
    from PIL import ImageFont

    epd = EPD4In2B()
    # For simplicity, the arguments are explicit numerical coordinates
    image_red = Image.new('1', (WIDTH, HEIGHT), 255)  # 255: clear the frame
    draw_red = ImageDraw.Draw(image_red)
    image_black = Image.new('1', (WIDTH, HEIGHT), 255)  # 255: clear the frame
    draw_black = ImageDraw.Draw(image_black)
    font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf', 24)
    draw_black.rectangle((0, 6, 400, 30), fill=0)
    draw_black.text((100, 10), 'e-Paper demo', font=font, fill=255)
    draw_black.arc((40, 80, 180, 220), 0, 360, fill=0)
    draw_red.rectangle((200, 80, 360, 280), fill=0)
    draw_red.arc((240, 80, 380, 220), 0, 360, fill=255)

    # display the frames
    epd.display_frame(epd.get_frame_buffer(image_black), epd.get_frame_buffer(image_red))

    # display images
    # frame_black = epd.get_frame_buffer(Image.open('black.bmp'))
    # frame_red = epd.get_frame_buffer(Image.open('red.bmp'))
    # epd.display_frame(frame_black, frame_red)
