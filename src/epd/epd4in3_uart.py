import os
import time
from serial import Serial
try:
    import RPi.GPIO as GPIO
except ImportError:
    pass


MAX_STRING_LEN          = 1024 - 4

# Prefix & Suffix of packet
FRAME_BEGIN             = "A5"
FRAME_END               = "CC33C33C"

# colours
COLORS = {
    'BLACK': '00',
    'DARK_GRAY': '01',
    'GRAY': '02',
    'WHITE': '03'
}

# commands
CMD_HANDSHAKE           = "00"  # Handshake
CMD_SET_BAUD            = "01"  # Set baud rate
CMD_READ_BAUD           = "02"  # Read baud rate
CMD_READ_MEMORYMODE     = "06"  # Read memory mode
CMD_SET_MEMORYMODE      = "07"  # Set memory mode
CMD_SLEEP               = "08"  # Enter stop mode
CMD_UPDATE              = "0A"  # Update
CMD_SCREEN_ROTATION     = "0D"  # Set screen rotation
CMD_LOAD_FONT           = "0E"  # Copy font files from SD card to NANDFlash.

# Font files include GBK32/48/64.FON.  48MB is allocated in NANDFlash for fonts.
# LED will flicker 3 times when starts and ends.
CMD_LOAD_PIC            = "0F"  # Import the image files from SD card to the NANDFlash.
# LED will flicker 3 times when starts and ends. 80MB allocated in NANDFlash for images
CMD_SET_COLOR           = "10"  # set colour
CMD_SET_EN_FONT         = "1E"  # set English font
CMD_SET_CH_FONT         = "1F"  # set Chinese font

CMD_DRAW_PIXEL          = "20"  # set pixel
CMD_DRAW_LINE           = "22"  # draw line
CMD_FILL_RECT           = "24"  # fill rectangle
CMD_DRAW_RECT           = "25"  # draw rectangle
CMD_DRAW_CIRCLE         = "26"  # draw circle
CMD_FILL_CIRCLE         = "27"  # fill circle
CMD_DRAW_TRIANGLE       = "28"  # draw triangle
CMD_FILL_TRIANGLE       = "29"  # fill triangle
CMD_CLEAR               = "2E"  # clear screen with background colour
CMD_DRAW_STRING         = "30"  # draw string
CMD_DRAW_BITMAP         = "70"  # draw bitmap

# Memory Mode
MEM_NAND                = "00"
MEM_SD                  = "01"


IS_RPI = True if 'arm' in os.uname()[4] else False


if IS_RPI:
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)


def ascii2hex(string):
    """
    ASCII string to Hex string. e.g. "World" => "576F726C64"
    :param string:
    :return:
    """
    hex_str = ""
    for c in string:
        hex_str = hex_str + hex(ord(c))[-2:]
    # Append "00" at the end of the string as required
    return hex_str + "00"


def hex2byte(hex_str):
    """
    hex string to bytes with parity byte at the end
    :param hex_str:
    :return:
    """
    byte_list = []
    parity = 0x00
    hex_str = hex_str.replace(" ", '')
    for i in range(0, len(hex_str), 2):
        byte = int(hex_str[i:i + 2], 16)
        byte_list.append(chr(byte))
        parity = parity ^ byte
    byte_list.append(chr(parity))
    return ''.join(byte_list)


def _hexify(i, digits=2):
    """
    Convert an integer into a hex value of a given number of digits
    :param i:
    :param digits:
    :return:
    """
    format_string = "0%dx" % digits
    return format(i, format_string).upper()


class EPD4In2UART:
    """
    Interface of 4.3" UART e-Paper from WaveShare
    """
    def __init__(self, port='/dev/ttyS0', baudrate=115200, wakeup_pin=23, rst_pin=24, timeout=1, verbose=False):
        self._ser = Serial(port=port, baudrate=baudrate, timeout=timeout)
        self.wakeup_pin = wakeup_pin
        self.rst_pin = rst_pin
        self.verbose = verbose
        if IS_RPI:
            GPIO.setup(self.rst_pin, GPIO.OUT)
            GPIO.setup(self.wakeup_pin, GPIO.OUT)
        self.init()

    def init(self):
        """
        Initialize e-Paper.
        :return:
        """
        if IS_RPI:
            # Set the wake_up pin low ready for a WAKE_UP command to be issued as LOW-HIGH-LOW
            GPIO.output(self.wakeup_pin, GPIO.LOW)
            # Set the pin low ready for a RESET command to be issued as LOW-HIGH-LOW
            GPIO.output(self.rst_pin, GPIO.LOW)

    def reset(self):
        """
        Reset e-paper
        :return:
        """
        if IS_RPI:
            GPIO.output(self.rst_pin, GPIO.LOW)
            time.sleep(.001)
            GPIO.output(self.rst_pin, GPIO.HIGH)
            time.sleep(.001)
            GPIO.output(self.rst_pin, GPIO.LOW)
            time.sleep(3)

    def wakeup(self):
        if IS_RPI:
            GPIO.output(self.wakeup_pin, GPIO.LOW)
            time.sleep(.001)
            GPIO.output(self.wakeup_pin, GPIO.HIGH)
            time.sleep(.001)
            GPIO.output(self.wakeup_pin, GPIO.LOW)
            time.sleep(.001)

    def update(self):
        """
        Refresh and update the display at once
        :return:
        """
        self._send_data(cmd=CMD_UPDATE)

    def clear(self):
        """
        Clear the screen with background color
        :return:
        """
        self._send_data(cmd=CMD_CLEAR)
        self.update()

    def handshake(self):
        """
        Handshake command. If the module is ready, it will return an "OK".
        :return:
        """
        return self._send_data(cmd=CMD_HANDSHAKE, ret_val=True)

    def set_baudrate(self, baudrate):
        """
        Set the serial Baud rate
        :param baudrate: Should be one of 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200
        :return:
        """
        rate = _hexify(baudrate, 8)
        self._send_data(cmd=CMD_SET_BAUD, data=rate)
        print "> Waiting for the EPD to re-initiate with new baud rate..."
        time.sleep(5)
        print "> Reconnecting with baud rate %d ..." % baudrate
        self._ser.baudrate = baudrate

    def read_baudrate(self):
        """
        Read the current baud rate
        :return: baudrate
        :rtype: int
        """
        val = self._send_data(cmd=CMD_READ_BAUD, ret_val=True)
        try:
            return int(val)
        except ValueError:
            pass

    def set_memory_type(self, _type='nand'):
        """
        Set type of the memory
        :param _type: `nand` or `sd`
        :return:
        """
        if _type == 'nand':
            self._send_data(cmd=CMD_SET_MEMORYMODE, data=MEM_NAND)
        elif _type == 'sd':
            self._send_data(cmd=CMD_SET_MEMORYMODE, data=MEM_SD)
        else:
            raise ValueError('Unexpected memory type')

    def get_memory_type(self):
        """
        Read current memory type.
        :return:
        """
        val = int(self._send_data(cmd=CMD_READ_MEMORYMODE, ret_val=True))
        if val == 0:
            return 'NAND'
        elif val == 1:
            return 'MicroSD'

    def set_screen_rotation(self, rotate=False):
        """
        Rotate screen or not.
        :param rotate: Rotate screen when True, and vice versa.
        :return:
        """
        self._send_data(cmd=CMD_SCREEN_ROTATION, data='01' if rotate else '00')

    def import_font(self):
        """
        Import font library: 48MB
        :return:
        """
        self._send_data(cmd=CMD_LOAD_FONT)

    def import_picture(self):
        """
        Import image: 80MB
        :return:
        """
        self._send_data(cmd=CMD_LOAD_PIC)

    def set_color(self, foreground='BLACK', background='WHITE'):
        """
        Set color of the e-Paper.
        Note: Colors should be one of `BLACK`, `DARK_GRAY`, `GRAY` and `WHITE`
        :param foreground:
        :param background:
        :return:
        """
        fg = COLORS.get(foreground, '00')
        bg = COLORS.get(background, '03')
        self._send_data(cmd=CMD_SET_COLOR, data=fg + bg)

    def set_font_size(self, font_type='en', size=32):
        """
        Set the type of font.
        :param font_type: `en` or `ch`
        :param size: Should be one of `32`, `48` and `64`
        :return:
        """
        size_data = {32: '01', 48: '02', 64: '03'}.get(size, '01')
        self._send_data(cmd=CMD_SET_EN_FONT if font_type == 'en' else CMD_SET_CH_FONT,
                        data=size_data)

    def pixel(self, x=0, y=0):
        """
        Draw a pixel
        :param x:
        :param y:
        :return:
        """
        self._send_data(cmd=CMD_DRAW_PIXEL, data=_hexify(x, 4) + _hexify(y, 4))

    def line(self, x0, y0, x1, y1):
        self._send_data(CMD_DRAW_LINE, _hexify(x0, 4) + _hexify(y0, 4) + _hexify(x1, 4) + _hexify(y1, 4))

    def rect(self, x0, y0, x1, y1):
        self._send_data(CMD_DRAW_RECT, _hexify(x0, 4) + _hexify(y0, 4) + _hexify(x1, 4) + _hexify(y1, 4))

    def fill_rect(self, x0, y0, x1, y1):
        self._send_data(CMD_FILL_RECT, _hexify(x0, 4) + _hexify(y0, 4) + _hexify(x1, 4) + _hexify(y1, 4))

    def circle(self, x, y, r):
        self._send_data(CMD_DRAW_CIRCLE, _hexify(x, 4) + _hexify(y, 4) + _hexify(r, 4))

    def fill_circle(self, x, y, r):
        self._send_data(CMD_FILL_CIRCLE, _hexify(x, 4) + _hexify(y, 4) + _hexify(r, 4))

    def triangle(self, x0, y0, x1, y1, x2, y2):
        self._send_data(
            CMD_DRAW_TRIANGLE,
            _hexify(x0, 4) + _hexify(y0, 4) + _hexify(x1, 4) + _hexify(y1, 4) + _hexify(x2, 4) + _hexify(y2, 4))

    def fill_triangle(self, x0, y0, x1, y1, x2, y2):
        self._send_data(
            CMD_FILL_TRIANGLE,
            _hexify(x0, 4) + _hexify(y0, 4) + _hexify(x1, 4) + _hexify(y1, 4) + _hexify(x2, 4) + _hexify(y2, 4))

    def text(self, x, y, txt):
        """
        Draw an ASCII character.
        :param x:
        :param y:
        :param txt:
        :return:
        """
        txt = txt[:MAX_STRING_LEN]
        self._send_data(CMD_DRAW_STRING, _hexify(x, 4) + _hexify(y, 4) + ascii2hex(txt))

    def chinese(self, x, y, gb2312_hex):
        """
        Draw Chinese character.
            "hello world" in Chinese: C4E3 BAC3 CAC0 BDE7
        """
        gb2312_hex = gb2312_hex.replace(" ", "") + "00"
        gb2312_hex = gb2312_hex[:MAX_STRING_LEN * 2]
        self._send_data(CMD_DRAW_STRING, _hexify(x, 4) + _hexify(y, 4) + gb2312_hex)

    def bitmap(self, x, y, name):
        """
        Draw bitmap image.
        :param x:
        :param y:
        :param name:
        :return:
        """
        self._send_data(CMD_DRAW_BITMAP, _hexify(x, 4) + _hexify(y, 4) + ascii2hex(name))

    def _send_data(self, cmd, data='', ret_val=False):
        """
        Compose a packet and send data to the e-Paper thru serial bus.
        :param cmd: Command byte, see Set the serial Baud rate for more details.
        :param data: Payload to be sent.
        :param ret_val: When True, waits for the response from the e-Paper.
        :return:
        """
        packet = FRAME_BEGIN + _hexify(9 + len(data) // 2, 4) + cmd + data + FRAME_END
        self._ser.write(hex2byte(packet))
        if self.verbose or ret_val:
            val = self._ser.readline()
            print('> {}'.format(val))
            return val


if __name__ == '__main__':
    epd = EPD4In2UART(verbose=True)
    # epd.reset()
    print('Handshake: {}'.format(epd.handshake()))
    print('Baudrate: {}'.format(epd.read_baudrate()))
    print('Memory type: {}'.format(epd.get_memory_type()))

    epd.clear()
    epd.line(100, 100, 400, 100)
    epd.set_font_size(size=64)
    epd.text(100, 300, '~~~Marry Christmas~~~')
    epd.update()
