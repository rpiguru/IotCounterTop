from base import EPDBase

# Display resolution
WIDTH = 128
HEIGHT = 296

# EPD2IN9 commands
DRIVER_OUTPUT_CONTROL = 0x01
BOOSTER_SOFT_START_CONTROL = 0x0C
GATE_SCAN_START_POSITION = 0x0F
DEEP_SLEEP_MODE = 0x10
DATA_ENTRY_MODE_SETTING = 0x11
SW_RESET = 0x12
TEMPERATURE_SENSOR_CONTROL = 0x1A
MASTER_ACTIVATION = 0x20
DISPLAY_UPDATE_CONTROL_1 = 0x21
DISPLAY_UPDATE_CONTROL_2 = 0x22
WRITE_RAM = 0x24
WRITE_VCOM_REGISTER = 0x2C
WRITE_LUT_REGISTER = 0x32
SET_DUMMY_LINE_PERIOD = 0x3A
SET_GATE_TIME = 0x3B
BORDER_WAVEFORM_CONTROL = 0x3C
SET_RAM_X_ADDRESS_START_END_POSITION = 0x44
SET_RAM_Y_ADDRESS_START_END_POSITION = 0x45
SET_RAM_X_ADDRESS_COUNTER = 0x4E
SET_RAM_Y_ADDRESS_COUNTER = 0x4F
TERMINATE_FRAME_READ_WRITE = 0xFF


# Display orientation
ROTATE_0 = 0
ROTATE_90 = 1
ROTATE_180 = 2
ROTATE_270 = 3


# Pin definition
RST_PIN = 5
DC_PIN = 6
BUSY_PIN = 13


LUT_FULL_UPDATE = [
        0x02, 0x02, 0x01, 0x11, 0x12, 0x12, 0x22, 0x22,
        0x66, 0x69, 0x69, 0x59, 0x58, 0x99, 0x99, 0x88,
        0x00, 0x00, 0x00, 0x00, 0xF8, 0xB4, 0x13, 0x51,
        0x35, 0x51, 0x51, 0x19, 0x01, 0x00
    ]

LUT_PARTIAL_UPDATE = [
    0x10, 0x18, 0x18, 0x08, 0x18, 0x18, 0x08, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x13, 0x14, 0x44, 0x12,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00
]


class EPD2In9(EPDBase):

    lut = None

    def __init__(self, spi_channel=1, width=WIDTH, height=HEIGHT, rotate=1,
                 rst_pin=RST_PIN, dc_pin=DC_PIN, busy_pin=BUSY_PIN):
        EPDBase.__init__(self, spi_channel=spi_channel, width=width, height=height,
                         rst_pin=rst_pin, dc_pin=dc_pin, busy_pin=busy_pin)
        self.rotate = rotate

    def start_full_drawing(self):
        """
        Start full drawing. This takes ~0.8 sec
        :return:
        """
        self._init(LUT_FULL_UPDATE)

    def start_partial_drawing(self):
        """
        Start full drawing. This takes ~0.8 sec
        :return:
        """
        self._init(LUT_PARTIAL_UPDATE)

    def _init(self, lut):
        # EPD hardware init start
        self.lut = lut
        self.reset()
        self.send_command(DRIVER_OUTPUT_CONTROL)
        self.send_data((HEIGHT - 1) & 0xFF)
        self.send_data(((HEIGHT - 1) >> 8) & 0xFF)
        self.send_data(0x00)  # GD = 0 SM = 0 TB = 0
        self.send_command(BOOSTER_SOFT_START_CONTROL)
        self.send_data(0xD7)
        self.send_data(0xD6)
        self.send_data(0x9D)
        self.send_command(WRITE_VCOM_REGISTER)
        self.send_data(0xA8)  # VCOM 7C
        self.send_command(SET_DUMMY_LINE_PERIOD)
        self.send_data(0x1A)  # 4 dummy lines per gate
        self.send_command(SET_GATE_TIME)
        self.send_data(0x08)  # 2us per line
        self.send_command(DATA_ENTRY_MODE_SETTING)
        self.send_data(0x03)  # X increment Y increment

        self.send_command(WRITE_LUT_REGISTER)
        # the length of look-up table is 30 bytes
        for i in range(0, len(lut)):
            self.send_data(self.lut[i])

    def _paste_image(self, image, x, y):
        """
        Put an image to the given position of the frame memory(regardless of the rotation).
        NOTE: This won't update the display.
                This takes ~1.2 sec
        :param image:
        :param x:
        :param y:
        :return:
        """
        if image is None or x < 0 or y < 0:
            return
        image_monocolor = image.convert('1')
        image_width, image_height = image_monocolor.size
        # x point must be the multiple of 8 or the last 3 bits will be ignored
        x = x & 0xF8
        image_width = image_width & 0xF8
        if x + image_width >= self.width:
            x_end = self.width - 1
        else:
            x_end = x + image_width - 1
        if y + image_height >= self.height:
            y_end = self.height - 1
        else:
            y_end = y + image_height - 1
        self._set_memory_area(x, y, x_end, y_end)
        self._set_memory_pointer(x, y)
        self.send_command(WRITE_RAM)
        # send the image data
        pixels = image_monocolor.load()
        byte_to_send = 0x00
        for j in range(0, y_end - y + 1):
            # 1 byte = 8 pixels, steps of i = 8
            for i in range(0, x_end - x + 1):
                # Set the bits for the column of pixels at the current position.
                if pixels[i, j] != 0:
                    byte_to_send |= 0x80 >> (i % 8)
                if i % 8 == 7:
                    self.send_data(byte_to_send)
                    byte_to_send = 0x00

    def paste_image(self, image, x, y):
        """
        Paste the partial image at the position. (Rotation is considered.)
        :param image:
        :param x:
        :param y:
        :return:
        """
        rotated_img = image.rotate(angle=-self.rotate * 90, expand=True)
        if self.rotate == 0:
            new_x, new_y = x, y
        elif self.rotate == 1:
            new_x, new_y = WIDTH - image.height - y, x
        elif self.rotate == 2:
            new_x, new_y = WIDTH - image.width - x, HEIGHT - image.height - y
        else:
            new_x, new_y = y, HEIGHT - image.width - x
        if new_x < 0 or new_y < 0:
            raise ValueError('Invalid coordinate of the image.')
        self._paste_image(rotated_img, new_x, new_y)

    def clear_frame_memory(self, color):
        """
        Clear the frame memory with the specified color.
        NOTE: This won't update the display.
                This takes ~0.8 sec
        :param color:
        :return:
        """
        self._set_memory_area(0, 0, self.width - 1, self.height - 1)
        self._set_memory_pointer(0, 0)
        self.send_command(WRITE_RAM)
        # send the color data
        for i in range(0, self.width / 8 * self.height):
            self.send_data(color)

    def render_frame(self):
        """
        Update the display.
            There are 2 memory areas embedded in the e-paper display, but once this function is called.
            The the next action of SetFrameMemory or ClearFrame will set the other memory area.
            This takes ~1.7 sec
        :return:
        """
        if self.lut is None:
            raise AttributeError('Cannot render a frame, '
                                 'please initialize with `start_full_drawing()` or `start_partial_drawing()`')
        self._busy = True
        self.send_command(DISPLAY_UPDATE_CONTROL_2)
        self.send_data(0xC4)
        self.send_command(MASTER_ACTIVATION)
        self.send_command(TERMINATE_FRAME_READ_WRITE)
        self.wait_until_idle()
        self._busy = False

    def _set_memory_area(self, x_start, y_start, x_end, y_end):
        """
        specify the memory area for data R/W
        :param x_start:
        :param y_start:
        :param x_end:
        :param y_end:
        :return:
        """
        self.send_command(SET_RAM_X_ADDRESS_START_END_POSITION)
        # x point must be the multiple of 8 or the last 3 bits will be ignored
        self.send_data((x_start >> 3) & 0xFF)
        self.send_data((x_end >> 3) & 0xFF)
        self.send_command(SET_RAM_Y_ADDRESS_START_END_POSITION)
        self.send_data(y_start & 0xFF)
        self.send_data((y_start >> 8) & 0xFF)
        self.send_data(y_end & 0xFF)
        self.send_data((y_end >> 8) & 0xFF)

    def _set_memory_pointer(self, x, y):
        """
        specify the start point for data R/W
        :param x:
        :param y:
        :return:
        """
        self.send_command(SET_RAM_X_ADDRESS_COUNTER)
        # x point must be the multiple of 8 or the last 3 bits will be ignored
        self.send_data((x >> 3) & 0xFF)
        self.send_command(SET_RAM_Y_ADDRESS_COUNTER)
        self.send_data(y & 0xFF)
        self.send_data((y >> 8) & 0xFF)
        self.wait_until_idle()

    def sleep(self):
        """
        After this command is transmitted, the chip would enter the deep-sleep mode to save power.
        The deep sleep mode would return to standby by hardware reset.
        You can use reset() to awaken or init() to initialize
        :return:
        """
        self.send_command(DEEP_SLEEP_MODE)
        self.wait_until_idle()

    def clear(self):
        """
        There are 2 memory areas embedded in the e-paper display and once the display is refreshed,
        the memory area will be auto-toggled,
        i.e. The next action of SetFrameMemory will set the other memory area, and
            therefore you have to set the frame memory twice.
        :return:
        """
        self.clear_frame_memory(0xFF)
        self.render_frame()
        self.clear_frame_memory(0xFF)
        self.render_frame()

    def display_image(self, image, x=0, y=0):
        self.paste_image(image, x, y)
        self.render_frame()


if __name__ == '__main__':
    import time
    from PIL import Image, ImageDraw, ImageFont

    s_time = time.time()
    epd = EPD2In9()
    epd.start_full_drawing()
    print 'Elapsed for initialization: ', time.time() - s_time
    s_time = time.time()
    # For simplicity, the arguments are explicit numerical coordinates
    _image = Image.new('1', (HEIGHT, WIDTH), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(_image)
    font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf', 16)
    draw.rectangle((0, 10, 128, 34), fill=0)
    draw.text((8, 12), 'Hello world!', font=font, fill=255)
    draw.text((8, 36), 'e-Paper Demo', font=font, fill=0)
    draw.line((16, 60, 56, 60), fill=0)
    draw.line((56, 60, 56, 110), fill=0)
    draw.line((16, 110, 56, 110), fill=0)
    draw.line((16, 110, 16, 60), fill=0)
    draw.line((16, 60, 56, 110), fill=0)
    draw.line((56, 60, 16, 110), fill=0)
    draw.arc((60, 90, 120, 150), 0, 360, fill=0)
    draw.rectangle((16, 130, 56, 180), fill=0)
    draw.chord((60, 160, 120, 220), 0, 360, fill=0)

    epd.clear_frame_memory(0xFF)
    epd.display_image(_image)

    print('Elapsed: {}'.format(time.time() - s_time))

    epd.clear()

    # for partial update
    epd.start_partial_drawing()

    time_image = Image.new('1', (150, 32), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(time_image)
    font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf', 32)
    _image_width, _image_height = time_image.size
    while True:
        # draw a rectangle to clear the image
        draw.rectangle((0, 0, _image_width, _image_height), fill=255)
        draw.text((0, 0), time.strftime('%H:%M:%S'), font=font, fill=0)
        epd.display_image(time_image, (HEIGHT - time_image.width) // 2, 48)
