import os
import logging.config

_cur_dir = os.path.dirname(os.path.realpath(__file__))

_log_dir = os.path.join(_cur_dir, 'log')
if not os.path.exists(_log_dir):
    os.makedirs(_log_dir)

logging.config.fileConfig(os.path.join(_cur_dir, 'logging.ini'))
logger = logging.getLogger('IoTCT')


# ============================= General Configuration ================================

DEBUG = False

# Interval of checking position (minutes)
GPS_CHECK_INTERVAL = 5

# WiFi QR Code scan time
WIFI_QR_SCAN_TIME = 5

CAMERA_RESOLUTION = (800, 600)
FACE_MIN_SIZE = (100, 100)

# Show live video from PiCamera?
SHOW_VIDEO = False


UPLOAD_FACE = True

# ============================= Service Configuration ================================

URL_BASE = 'https://dev.services.32point6.com/'

URL_CONFIG = 'api/v1/admin/iot/config/'
URL_LICENCE = 'api/v1/admin/iot/license/'
URL_CHECKOUT = 'api/v2/shopping/iot/checkout/complete'

AUTH_CODE = '000#IBVhW0iAPr:A+EA^gIpVeoh{4es3*wuoMJ3N{tG$&sh-lRJ1%gu523fg6Kd>Im5Lx[.vD$UT5/' \
            'E0J>W7%VEfL6S4zPHtoRWKSdEGO6.n7]T+vvJxh$A+E%WlI1f-Ze<GZ.aoYS(b'

# ============================= Azure IoT Settings ===================================
# messageTimeout - the maximum time in milliseconds until a message times out.
# The timeout period starts at IoTHubClient.send_event_async.
# By default, messages do not expire.
AZURE_IOT_MESSAGE_TIMEOUT = 10000

AZURE_IOT_RECEIVE_CONTEXT = 0
AZURE_IOT_TWIN_CONTEXT = 0
AZURE_IOT_METHOD_CONTEXT = 0


# ============================== Pin Connection ===========================================

BUTTON_PIN = 25      # Pin Number(BCM) of the keypad button

EPD4in2B_RST_PIN = 17
EPD4in2B_DC_PIN = 27
EPD4in2B_BUSY_PIN = 22
EPD4in2B_ROTATION = 0

EPD2in9_TYPE = '2in9'       # `2in9` or `2in9b`

EPD2in9B_RST_PIN = 5
EPD2in9B_DC_PIN = 6
EPD2in9B_BUSY_PIN = 13
EPD2in9B_ROTATION = 1       # 0: 0 deg, 1: 90 deg, 2: 180 deg, 3: 270 deg

try:
    from local_settings import *
except ImportError:
    pass
