#!/usr/bin/env bash
echo "===== Installing MS Azure IoT Hub Client ====="

sudo apt-get install -y libboost-python1.62-dev
cur_dir=`dirname $0`

revision_code=$(sudo cat /proc/cpuinfo | grep 'Revision' | awk '{print $3}' | sed 's/^ *//g' | sed 's/ *$//g')

if [ "$revision_code" = "0x9000C1" ]; then      # Raspberry Pi Zero
    sudo cp ${cur_dir}/iothub_client_zero.so /usr/local/lib/python2.7/dist-packages/iothub_client.so
else
    sudo cp ${cur_dir}/iothub_client_3.so /usr/local/lib/python2.7/dist-packages/iothub_client.so
fi

#echo "== Temporarily Increasing SWAP file size =="
#sudo sed -i -- 's/CONF_SWAPSIZE=100/CONF_SWAPSIZE=1536/g' /etc/dphys-swapfile
#sudo /etc/init.d/dphys-swapfile stop
#sudo /etc/init.d/dphys-swapfile start
#
#cd ~
#git clone https://github.com/Azure/azure-iot-sdk-python.git --recursive
#cd azure-iot-sdk-python/build_all/linux
#./setup.sh
#./build.sh
#sudo cp ../../device/samples/iothub_client.so /usr/local/lib/python2.7/dist-packages
#cd ~
#sudo rm -r azure-iot-sdk-python
#sudo sed -i -- 's/CONF_SWAPSIZE=1536/CONF_SWAPSIZE=100/g' /etc/dphys-swapfile
#sudo /etc/init.d/dphys-swapfile stop
#sudo /etc/init.d/dphys-swapfile start
