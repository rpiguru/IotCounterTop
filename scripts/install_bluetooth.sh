#!/usr/bin/env bash

sudo apt-get install -y libusb-dev
sudo apt-get install -y libdbus-1-dev
sudo apt-get install -y libglib2.0-dev --fix-missing
sudo apt-get install -y libudev-dev
sudo apt-get install -y libical-dev
sudo apt-get install -y libreadline-dev

cd ~
wget www.kernel.org/pub/linux/bluetooth/bluez-5.18.tar.gz
gunzip bluez-5.18.tar.gz
tar xvf bluez-5.18.tar
cd bluez-5.18
./configure --disable-systemd
make -j2
sudo make install
cd ~
rm bluez-5.18.tar.gz
rm bluez-5.18.tar
sudo rm -r bluez-5.18
