#!/usr/bin/env bash
echo "===== Installing GPS service ====="
sudo echo "enable_uart=1" | sudo tee -a /boot/config.txt
sudo sed -i -- 's/console=serial0,115200 //g' /boot/cmdline.txt
sudo sed -i -- 's/console=ttyAMA0,115200 //g' /boot/cmdline.txt
sudo sed -i -- 's/kgdboc=ttyAMA0,115200 //g' /boot/cmdline.txt
sudo sed -i -- 's/console=ttyS0,115200 //g' /boot/cmdline.txt
sudo systemctl stop serial-getty@ttyS0.service
sudo systemctl disable serial-getty@ttyS0.service

sudo usermod -a -G dialout pi
