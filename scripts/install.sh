#!/usr/bin/env bash

echo "===== Starting Installation scripts of IoTCounterTop ====="
sudo apt-get update
sudo apt-get purge -y wolfram-engine
sudo apt-get purge -y libreoffice*
sudo apt-get clean
sudo apt-get autoremove -y
sudo apt-get install -y cmake build-essential curl libcurl4-openssl-dev libssl-dev uuid-dev pkg-config
sudo apt-get install -y python-dev python-smbus python-spidev libzbar0 freetds-dev

sudo pip install -U pip
sudo pip install requests pynmea2 numpy pyqrcode pypng applicationinsights netifaces pyBarcode pyzbar

echo "===== Enabling SPI and Camera ====="
sudo echo "dtparam=spi=on" | sudo tee -a /boot/config.txt
sudo echo "dtoverlay=spi1-1cs" | sudo tee -a /boot/config.txt
sudo echo "start_x=1" | sudo tee -a /boot/config.txt

echo "== Installing NetworkManager =="
sudo apt-get install -y network-manager
sudo sed -i -- 's/managed=false/managed=true/g' /etc/NetworkManager/NetworkManager.conf
sudo echo "denyinterfaces wlan0" | sudo tee -a /etc/dhcpcd.conf

# Disable wlan0 configuration for the NetworkManager
sudo sed -i -- 's/allow-hotplug wlan0//g' /etc/network/interfaces
sudo sed -i -- 's/iface wlan0 inet/#iface wlan0 inet/g' /etc/network/interfaces
sudo sed -i -- 's/wpa-conf/#wpa-conf/g' /etc/network/interfaces
sudo sed -i -- 's/wpa-ssid/#wpa-ssid/g' /etc/network/interfaces
sudo sed -i -- 's/wpa-psk/#wpa-psk/g' /etc/network/interfaces

cur_dir=`dirname $0`
bash ${cur_dir}/install_gps.sh
bash ${cur_dir}/install_bluetooth.sh
bash ${cur_dir}/install_iothub.sh
bash ${cur_dir}/install_opencv.sh
